/**
 * Created by yuezhang on 4/22/15.
 */
module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        files: [{
          expand: true,
          cwd: 'app/public/sass',
          src: ['*.scss'],
          dest: 'app/public/css',
          ext: '.css'
        }]
      }
    },
    //,cssmin: {
    //  style:{
    //    src: 'app/css/style.css',
    //    dest: 'style.min.css'
    //  }
    //}
    watch: {
      css: {
        files: 'app/public/sass/*.scss',
        tasks: ['sass']
      }
      //,cssmin: {
      //  files: '**/**/*.css',
      //  tasks: ['cssmin']
      //}
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default',['watch']);

  grunt.registerTask('default', [ 'sass:dist' ]);
};