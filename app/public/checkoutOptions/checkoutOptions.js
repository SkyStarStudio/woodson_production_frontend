(function(){
  'use strict';

  angular.module('ecommerce.checkoutOptions', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/checkoutOptions', {
        templateUrl: 'checkoutOptions/checkoutOptions.html',
        controller: 'CheckoutOptionsCtrl'
      });
    }])

    .controller('CheckoutOptionsCtrl', [function () {

    }]);
})();