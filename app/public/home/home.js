/**
 * Created by yuezhang on 4/21/15.
 */
(function(){
  'use strict';

  angular.module('ecommerce.home', [
    'ngRoute'
  ])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/home', {
        templateUrl: 'home/home.html',
        controller: 'HomeCtrl'
      });
    }])

    .controller('HomeCtrl', [function () {

    }]);
})();