(function(){
  'use strict';

  angular.module('ecommerce.account', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/account', {
        templateUrl: 'account/account.html',
        controller: 'AccountCtrl'
      });
    }])

    .controller('AccountCtrl', ['$scope', function ($scope) {

    }]);
})();