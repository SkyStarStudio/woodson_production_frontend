(function(){
  'use strict';

  angular.module('ecommerce.productDetail', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/productDetail', {
        templateUrl: 'productDetail/productDetail.html',
        controller: 'ProductDetailCtrl'
      });
    }])

    .controller('ProductDetailCtrl', [function () {

    }]);
})();