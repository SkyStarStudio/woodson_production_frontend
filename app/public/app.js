(function(){
  'use strict';

// Declare app level module which depends on views, and components
  angular.module('ecommerce', [
    'ngRoute',
    'angular-flexslider',
    'ecommerce.home',
		'ecommerce.account',
    'ecommerce.register',
    'ecommerce.productsList',
		'ecommerce.categoriesList',
    'ecommerce.productDetail',
    'ecommerce.shoppingCart',
    'ecommerce.userInfo',
    'ecommerce.checkout',
    'ecommerce.checkoutOptions',
    'ecommerce.404'
  ])

    .directive('ecommerceHeader', function(){
      return {
        restrict: 'E',
        templateUrl: 'partials/header.html',
        replace: true
      };
    })

    .directive('ecommerceFooter', function(){
      return {
        restrict: 'E',
        templateUrl: 'partials/footer.html',
        replace: true
      };
    })

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider

        .when('/', {
          redirectTo: '/home',
        })

        .otherwise({
          redirectTo: '/404'
        });
    }]);
})();