(function(){
  'use strict';

  angular.module('ecommerce.categoriesList', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/categoriesList', {
        templateUrl: 'categoriesList/categoriesList.html',
        controller: 'CategoriesListCtrl'
      });
    }])

    .controller('CategoriesListCtrl', [function () {

    }]);
})();