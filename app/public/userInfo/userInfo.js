(function(){
  'use strict';

  angular.module('ecommerce.userInfo', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/userInfo', {
        templateUrl: 'userInfo/userInfo.html',
        controller: 'UserInfoCtrl'
      });
    }])

    .controller('UserInfoCtrl', [function () {

    }]);
})();