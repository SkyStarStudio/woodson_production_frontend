(function(){
  'use strict';

  angular.module('ecommerce.checkout', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/checkout', {
        templateUrl: 'checkout/checkout.html',
        controller: 'CheckoutCtrl'
      });
    }])

    .controller('CheckoutCtrl', [function () {

    }]);
})();