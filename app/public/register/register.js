(function(){
  'use strict';

  angular.module('ecommerce.register', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/register', {
        templateUrl: 'register/register.html',
        controller: 'RegisterCtrl'
      });
    }])

    .controller('RegisterCtrl', [function () {

    }]);
})();