$(document).ready(function () {
	
	var showContent = function(dom, event) {
		
		if (! dom.hasClass('is-active')) {
      event.preventDefault();

      $('.accordion-tab-headers').find('.is-active').removeClass('is-active');
      $('.accordion-tab-contents').children('.is-open').removeClass('is-open').hide();

      dom.addClass('is-active');
    } else {
      event.preventDefault();
    }
		
		var name = dom.parent().attr('name');
		$(".accordion-tab-contents").children("[name='" + name + "']").toggleClass('is-open').toggle();
	};

  var next = function(event) {
    event.preventDefault();

    var next = $('.accordion-tab-headers').find('.is-active').parent().next().find('.tab-link');
    showContent(next ,event);
  };

  $('.accordion-tab-headers').on('click', '.tab-link', function(event) {
		showContent($(this), event);
	}); 

  $("button[name='shipping']").on('click', {}, next);

  $("button[name='billing']").on('click', {}, next);

  showContent($('.accordion-tab-headers').find(".is-active"), event);

/*   var formData = JSON.stringify($("#myForm").serializeArray());
  $.ajax({
    type: "POST",
    url: "serverUrl",
    data: formData,
    success: function(){},
    dataType: "json",
    contentType : "application/json"
  }); */
});

